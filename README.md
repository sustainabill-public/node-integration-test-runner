# Integration test runner for gitlab

## Why should I use this project?

This Dockerfile builds an environment based on the yarn debian image to run integration tests (e.g. cypress).

If you use gitlab to build and deploy your code to some test server AND you want to run interation tests against this server, this project will help you.

Please note that this was tested only with cypress so far. If you use other test frameworks, you might need to adapt the Dockerfile.

## How to use the image?

To run your tests with this image you need to add a stage in your `gitlab-ci.yaml` file.

```
my integration test:
  image: registry.gitlab.com/sustainabill-public/cicd-runner
  stage: YOUR_STAGE
  script:
    - yarn test:headless --env YOUR_ENV
  only:
    - master
```

with:

* `YOUR_STAGE`: A stage that runs after you have deployed your code to some location accessible from your gitlab instance
* `YOUR_ENV`: This is optional. Likely your Javascript code will have some mechanism to point the test runner to the correct environment

## How to adapt this to my needs?

Essentially you need to adapt the `Dockerfile` and the `Makefile` of this project to create your own images.
The following could be adapted

* `Dockerfile`
  * Dependencies
  * Browser(s)
  * Base image version (e.g. debian version)
* `Makefile`
  * Registry
  * Architecture

## Troubelshooting

* The image cannot execute my `before_script`s
  * Add a line saying `before_script:` with some noop `echo` command to overwrite your standard script
  or move your before scripts to the stages where they are actually needed
* The image does not run my tests, but it works on my local machine (tm)
  * Usually it is enough if the versions you are running locally and the ones in this image are roughly the same.
    Make sure to rebuild the image to install recent versions of cypress, etc. whenever you update your frontend dependencies.
    If that does not work you might want to fix the versions.
