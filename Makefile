.DEFAULT_GOAL := image_amd64

image_amd64:
	# Build explicitly for amd64 to be able to run the image on gitlab
	docker build --platform linux/amd64 -t registry.gitlab.com/sustainabill-public/node-integration-test-runner .
	docker push registry.gitlab.com/sustainabill-public/node-integration-test-runner
image:
	# Build for your hardware, use this to run the package locally, especially on ARM hardware (e.g. M1/M2/...) 
	docker build -t registry.gitlab.com/sustainabill-public/node-integration-test-runner .
	# docker push registry.gitlab.com/sustainabill-public/node-integration-test-runner
