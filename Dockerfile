from node:bullseye

LABEL version="1.0"
LABEL description="This image is used to run the sustainabill cypress integration tests in gitlabs CI pipelines.\
It is derived from node:bullseye and adds chromium, chromedriver and nightwatch to be able to run integration tests"
LABEL maintainer="Thorsten Merten <thorsten.merten@sustainabill.de>"

# install Chromium and Chrome Engine
RUN apt-get update && apt-get install -y chromium

RUN yarn add -D chromedriver cypress @vue/cli-service @vue/cli-plugin-e2e-cypress @vue/cli-plugin-babel @vue/cli-plugin-eslint vue-cli-plugin-webpack-bundle-analyzer babel-plugin-transform-remove-console
ENV PATH="/node_modules/.bin/:$PATH"

# chomedriver is installed with npm/yarn anyways
# RUN apt-get update &&\
# apt-get install -y unzip curl chromium &&\
# wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip &&\
# unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

#CMD ["yarn", "install"]
